#!/bin/bash
#set -e
###############################################################################
# Author	:	Erik Dubois
# Website	:	https://www.erikdubois.be
# Website	:	https://www.arcolinux.info
# Website	:	https://www.arcolinux.com
# Website	:	https://www.arcolinuxd.com
# Website	:	https://www.arcolinuxb.com
# Website	:	https://www.arcolinuxiso.com
# Website	:	https://www.arcolinuxforum.com
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################

tput setaf 1;echo "################################################################"
echo "System update"
echo "################################################################"
echo;tput sgr0
	sudo pacman -Syu --noconfirm

###############################################################################
#
#   DECLARATION OF FUNCTIONS
#
###############################################################################

func_install_pacman() {
	if pacman -Qi $1 &> /dev/null; then
		tput setaf 2
		echo "###############################################################################"
		echo "################## The package " $1 " is already installed"
		echo "###############################################################################"
		echo
		tput sgr0
	else
		tput setaf 3
		echo "###############################################################################"
		echo "##################  Installing package " $1 
		echo "###############################################################################"
		echo
		tput sgr0
		sudo pacman -S --noconfirm --needed $1
	fi
}

func_install_aur() {
	if yay -Qi $1 &> /dev/null; then
		tput setaf 2
		echo "###############################################################################"
		echo "################## The aur package " $1 " is already installed"
		echo "###############################################################################"
		echo
		tput sgr0
	else
		tput setaf 4
		echo "###############################################################################"
		echo "##################  Installing aur package " $1 
		echo "###############################################################################"
		echo
		tput sgr0
		echo "1" | yay -S --noconfirm --needed  $1
	fi
}
###############################################################################
echo "Installation of the core software"
###############################################################################

sway_list=(
	sway
	swayidle
	swaylock
	waybar
	autotiling
	mako
	dmenu
	wofi
	slurp
	alacritty
	qt5-wayland
	xorg-xwayland
	xdg-desktop-portal-wlr
	#network-manager-applet
)

gtk_list=(
	polkit-gnome
	gnome-themes-extra
	#gnome-control-center
	#gnome-online-accounts
	#gvfs-google
	xdg-user-dirs-gtk
	xdg-desktop-portal-gtk
)

audio_list=(
	pulseaudio-alsa
	pavucontrol
	alsa-firmware
	alsa-lib
	alsa-plugins
	alsa-utils
	gstreamer
	gst-plugins-good
	gst-plugins-bad
	gst-plugins-base
	gst-plugins-ugly
	playerctl
	blueman
)

apps_list=(
	evolution
	firefox-adblock-plus
	kodi-wayland
	pamac-aur
	#pcmanfm-gtk3
	thunar-gtk3
	thunar-archive-plugin-gtk3
	file-roller
	qemu
	sublime-text-dev
	#termite
	transmission-gtk
	virt-manager
	#xarchiver
	grml-zsh-config
	zsh-autosuggestions
	zsh-completions
	zsh-syntax-highlighting
	#oh-my-zsh-git
	yay
)

font_list=(
	font-manager-git
	adobe-source-sans-pro-fonts
	ttf-font-awesome
	ttf-dejavu
	ttf-roboto
	ttf-ubuntu-font-family
)

theme_list=(
	gcolor3
	lxappearance
	kvantum-qt5
	sardi-colora-variations-icons-git
	surfn-icons-git
)

aur_list=(
	azote
	nwg-launchers
	#realvnc-vnc-viewer
	stremio
)

sway_count=0

for sway_name in "${sway_list[@]}" ; do
	sway_count=$[count+1]
	tput setaf 3;echo "Installing package  "$sway_count " " $sway_name;tput sgr0;
	func_install_pacman $sway_name
done

gtk_count=0

for gtk_name in "${gtk_list[@]}" ; do
  gtk_count=$[count+1]
  tput setaf 3;echo "Installing package  "$gtk_count " " $gtk_name;tput sgr0;
  func_install_pacman $gtk_name
done

audio_count=0

for audio_name in "${audio_list[@]}" ; do
  audio_count=$[count+1]
  tput setaf 3;echo "Installing package  "$audio_count " " $audio_name;tput sgr0;
  func_install_pacman $audio_name
done

apps_count=0

for apps_name in "${apps_list[@]}" ; do
  apps_count=$[count+1]
  tput setaf 3;echo "Installing package  "$apps_count " " $apps_name;tput sgr0;
  func_install_pacman $apps_name
done

font_count=0

for font_name in "${font_list[@]}" ; do
  font_count=$[count+1]
  tput setaf 3;echo "Installing package  "$font_count " " $font_name;tput sgr0;
  func_install_pacman $font_name
done

theme_count=0

for theme_name in "${theme_list[@]}" ; do
  theme_count=$[count+1]
  tput setaf 3;echo "Installing package  "$theme_count " " $theme_name;tput sgr0;
  func_install_pacman $theme_name
done

aur_count=0

for aur_name in "${aur_list[@]}" ; do
	aur_count=$[count+1]
	tput setaf 5;echo "Installing package  "$aur_count " " $aur_name;tput sgr0;
	func_install_aur $aur_name
done

###############################################################################

tput setaf 6;echo "################################################################"
echo "Enabling services"
echo "################################################################"
echo;tput sgr0

	sudo systemctl enable bluetooth.service
	sudo systemctl start bluetooth.service
	sudo sed -i 's/'#AutoEnable=false'/'AutoEnable=true'/g' /etc/bluetooth/main.conf

	sudo systemctl enable libvirtd 
	sudo systemctl start libvirtd 

tput setaf 7;echo "################################################################"
echo "Copying all files and folders from sway-arch to Home dir."
echo "################################################################"
echo;tput sgr0

	sudo rm -r ~/sway-arch/.git
	cp -r ~/sway-arch/.*  ~/ 2> /dev/null

tput setaf 7;echo "################################################################"
echo "Change shell to zsh"
echo "################################################################"
echo;tput sgr0

	chsh -s /bin/zsh
	sudo chsh -s /bin/zsh

tput setaf 8;echo "################################################################"
echo "Install done Reboot your system"
echo "################################################################"
echo;tput sgr0
