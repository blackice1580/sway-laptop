// =============================================================================
//
// Waybar configuration
//
// Configuration reference: https://github.com/Alexays/Waybar/wiki/Configuration
//
// =============================================================================

{
    // -------------------------------------------------------------------------
    // Global configuration
    // -------------------------------------------------------------------------

    "layer": "top",

    "position": "top",

    // If height property would be not present, it'd be calculated dynamically
    "height": 36,

    "modules-left": [
        "custom/menu",
        "custom/sep",
        "sway/workspaces",
        "sway/mode",
        "custom/sep",
        "sway/window"
    ],

    "modules-center": [
        "clock"
    ],

    "modules-right": [
        "idle_inhibitor",
        "cpu",
        "backlight",
        "network",
        "pulseaudio",
        "battery",
        "custom/sep",
        "custom/close"
    ],


    // -------------------------------------------------------------------------
    // Modules
    // -------------------------------------------------------------------------
    "custom/sep": {
        "format": "  |  "
    },
    "custom/close": {
        "format": "    ",
        "on-click": "swaymsg kill"
    },    
    "custom/power_menu": {
        "format": "    ",
        "on-click": "nwgbar"
    },
    "custom/menu": {
        "format": "  ",
        "on-click": "nwggrid -p -n 4"
    },
    "battery": {
        "interval": 30,
        "states": {
            "warning": 20,
            "critical": 10
        },
        "format-charging": "{icon} {capacity}%", // Icon: bolt
        "format": "{icon} {capacity}%",
        "format-icons": [
            "", // Icon: battery-empty
            "", // Icon: battery-quarter
            "", // Icon: battery-half
            "", // Icon: battery-three-quarters
            ""  // Icon: battery-full
        ],
        "tooltip": false,
        "on-click": "nwgbar"
    },

    "clock": {
      "interval": 60,
      "format": "{:%e %b %I:%M}", // Icon: calendar-alt
      "tooltip": false
    },

    "cpu": {
        "interval": 5,
        "format": " {usage}%", // Icon: microchip
        "states": {
          "warning": 70,
          "critical": 90
        },
        "on-click": "alacritty -e htop"
    },

    "idle_inhibitor": {
        "format": "  {icon}  ",
        "format-icons": {
            "activated": "  ",
            "deactivated": "  "
        }
    },

    "memory": {
        "interval": 5,
        "format": "  {}%", // Icon: memory
        "states": {
            "warning": 70,
            "critical": 90
        },
        "on-click": "alacritty -e htop"
    },

    "network": {
        "interval": 5,
        "format-wifi": "  {signalStrength}%", // Icon: wifi
        "format-ethernet": "  {ifname}:{ipaddr}/{cidr}", // Icon: ethernet
        "format-disconnected": " ⚠ Disconnected",
        "tooltip-format": "{ifname}:{ipaddr}",
        "on-click": "nm-connection-editor"
    },

    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>",
        "tooltip": false
    },

    "sway/window": {
        "format": "  {}  ",
        "max-length": 120
    },

    "sway/workspaces": {
        "all-outputs": false,
        "disable-scroll": true,
        "format": "{name}"
    },

    "backlight": {
    	"format": " {icon} {percent}%",
	"format-icons": ["", ""],
        "on-scroll-down": "light -A 1",
        "on-scroll-up": "light -U 1"
    },

    "pulseaudio": {
        //"scroll-step": 1,
        "format": "{icon} {volume}%",
        "format-bluetooth": "{icon} {volume}%",
        "format-muted": "",
        "format-icons": {
            "headphones": "",
            "handsfree": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", ""]
        },
        "on-click": "pavucontrol"
    },

    "temperature": {
        "thermal-zone": 1,
        "critical-threshold": 80,
        "interval": 5,
        "format": " {icon}  {temperatureC}°C ",
        "format-icons": [
          "", // Icon: temperature-empty
          "", // Icon: temperature-quarter
          "", // Icon: temperature-half
          "", // Icon: temperature-three-quarters
          ""  // Icon: temperature-full
        ],
        "tooltip": true
    },

    "tray": {
        "icon-size": 22
    }
}
