#!/bin/sh

# Session
export MOZ_ENABLE_WAYLAND=1
export _JAVA_AWT_WM_NONREPARENTING=1

sway --my-next-gpu-wont-be-nvidia
